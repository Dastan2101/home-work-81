import { FETCH_URL_SUCCESS} from "../actions/Actions";

const initialState = {
    shortUrl : {},
};

const Reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_URL_SUCCESS:
            return {...state, shortUrl: action.data};

        default:
            return state;
    }
};

export default Reducer;