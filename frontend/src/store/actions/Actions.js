import axios from 'axios';

export const FETCH_URL_SUCCESS = 'FETCH_URL_SUCCESS';


export const fetchUrlSuccess = data => ({type: FETCH_URL_SUCCESS, data});

export const sendUrl = (data) => {
    return dispatch => {
        return axios.post('http://localhost:8000/links', data).then(response => {
            dispatch(fetchUrlSuccess(response.data))
        })
    }
};
