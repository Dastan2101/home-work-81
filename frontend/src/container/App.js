import React, {Component, Fragment} from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import {sendUrl} from "../store/actions/Actions";
import {connect} from "react-redux";
import Icon from "@material-ui/core/Icon/Icon";
import Snackbar from "@material-ui/core/Snackbar/Snackbar";

const styles = theme => ({

    margin: {
        margin: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    }
});

class App extends Component {

    state = {
        url: '',
        open: false,
        vertical: 'top',
        horizontal: 'right',
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    handleClose = () => {
        this.setState({open: false});
    };

    sendOriginalUrl = () => {
        if (this.state.url !== '') {
            const data = {
                originalUrl: this.state.url
            };

            this.props.sendUrl(data);
            this.setState({open: true});
        }

    };


    render() {
        const {classes} = this.props;
        const {vertical, horizontal, open} = this.state;

        return (
            <Fragment>
                <div className="App">
                    <h1 style={{color: 'green'}}>Shorten your link!</h1>
                    <div className={classes.margin}>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <TextField id="input-with-icon-grid" label="Link" onChange={this.inputChangeHandler}
                                           name="url"
                                           type="url"
                                           required/>
                            </Grid>
                        </Grid>
                    </div>
                    <Button variant="contained" color="primary" className={classes.button}
                            onClick={this.sendOriginalUrl}>
                        Shorten
                        <Icon className={classes.rightIcon}>send</Icon>
                    </Button>
                    <h4 style={{color: 'red'}}>Your link now looks like this: </h4>
                    {this.props.shortUrl.shortUrl ?
                        <a href={'http://localhost:8000/' + this.props.shortUrl.shortUrl}>http://localhost:8000/{this.props.shortUrl.shortUrl}</a>
                        : null}
                </div>
                <Snackbar
                    anchorOrigin={{vertical, horizontal}}
                    open={open}
                    onClose={this.handleClose}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">Success</span>}
                />
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    shortUrl: state.shortUrl
});

const mapDispatchToProps = dispatch => ({
    sendUrl: (data) => dispatch(sendUrl(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));
