const express = require('express');
const cors = require('cors');
const shorter = require('./app/shorter');
const mongoose = require('mongoose');
const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect('mongodb://localhost/shorter', {useNewUrlParser: true}).then(() => {
    app.use('/', shorter);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

});

