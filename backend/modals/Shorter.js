const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ShorterSchema = new Schema({
    originalUrl: {
        type: String, required: true
    },
    shortUrl: {
        type: String, required: true
    }
});

const Shorter = mongoose.model('Shorter', ShorterSchema);

module.exports = Shorter;