const express = require('express');
const nanoid = require('nanoid');
const Shorter = require('../modals/Shorter');
const router = express.Router();

router.get('/:shortUrl', (req, res) => {
    Shorter.findOne({shortUrl: req.params.shortUrl})
        .then(result => {
            if (result) return res.status(301).redirect(result.originalUrl);
            res.sendStatus(404);
        })
        .catch(() => res.send(500));
});

router.post('/links', (req, res) => {
    let data = req.body;

    data.shortUrl = nanoid(6);

    const short = new Shorter(data);

    short.save()
        .then(result => res.send(result))
        .catch(error => res.sendStatus(error));

});

module.exports = router;
